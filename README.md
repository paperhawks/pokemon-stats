# Pokemon Go Simple Analysis
Creating a project to do some basic analysis for Pokemon Go.

The basic analysis is within the jupyter notebook "Pokemon Stats.ipynb".

As part of the notebook it uses some functions I wrote in cp_stats.py. 

This repository should already contain the datasets that I am using to make this analysis. If, however, that the datasets don't exist, one should be able to grab the Pokemon dataset from Kaggle. The moveset dataset was scapred from the web via the grab_charge_moves.py. Currently, there's not much that's useful in that dataset so I'm working on grab_pvp_moves.py, which will grab a more extensive dataset on both charge and fast moves along with the turns taken to deal said damage.
