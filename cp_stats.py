import numpy as np

# formulae from https://pokemongohub.net/post/meta/pokemon-go-cp-rework-stat-changes-formula-and-raiding-after-the-rework/ 

def old_round(val):
    """
    Creates python2's version of round that's needed for Pokemon Go.
    Rounds 0.5 to 1 instead of the closest even integer.

    Input:
        val - float 
    Output:
        rounded value - float - rounds to the next value if 0.5
    """
    if val-np.floor(val) < 0.5:
        return np.floor(val)
    else:
        return np.floor(val)+1

def modify_stat(stat):
    """
    Applies a 9% nerf on a given stat.

    Input:
        stat - numpy array of float - original Pokemon Go's base stat
    Output:
        numpy array of float - 9% nerf with rounding
    """
    vec_round = np.vectorize(old_round, otypes=[float])
    stat = stat * 0.91
    return vec_round(stat)

def base_stamina(hp):
    """
    Gives the base stamina of a Pokemon based off the HP.
    Input: 
    HP - base HP from the original series
    
    Output:
    Base stamina value used for Pokemon Go.
    """
    return np.floor(hp*1.75+50)

def base_atatck(att, sp_att, speed):
    """
    Gives the base attack of a Pokemon based on original series stats.

    Input:
        att - Original series attack
        ap_att - Original series special attack
        speed - Original series speed


    Output:
        Base attack of Pokemon in Pokemon Go 
    """
    vec_max = np.vectorize(max, otypes=[float])
    vec_min = np.vectorize(min, otypes=[float])
    vec_round = np.vectorize(old_round, otypes=[float])
    higher = vec_max(att, sp_att)
    lower = vec_min(att, sp_att)
    scaled_atack = vec_round(2 * (7./8*higher+1./8*lower))
    speed_mod = 1 + (speed-75)/500.
    return vec_round(scaled_atack*speed_mod)

def base_defense(defense, sp_defense, speed):
    """
    Gives the base defense of a Pokemon. 

    Input:
        defense - Original series defense
        sp_defense - Original series defense
        speed - Original series speed

    Output:
        Base defense of Pokemon in Pokemon Go
    """
    vec_max = np.vectorize(max, otypes=[float])
    vec_min = np.vectorize(min, otypes=[float])
    vec_round = np.vectorize(old_round, otypes=[float])
    higher = vec_max(defense, sp_defense)
    lower = vec_min(defense, sp_defense)
    scaled_defense = vec_round(2*(5./8*higher + 3./8*lower))
    speed_mod = speed_mod = 1 + (speed-75)/500.
    return vec_round(scaled_defense*speed_mod)
