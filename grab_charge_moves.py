import requests
from bs4 import BeautifulSoup
import pandas
import lxml.html
import os

# Get URL
url='https://gamepress.gg/pokemongo/pvp-charge-moves'
page = requests.get(url)

#Grab HTML table data
soup = BeautifulSoup(page.content, 'html.parser')
results = soup.find('table', attrs={"class":"views-table views-view-table cols-4"})
table_data = results.tbody.find_all("tr")
print(type(table_data))
root = lxml.html.fromstring(str(table_data))
moves_text = root.xpath('//tr//td//text()')
grouping_size = 5

#Save table data
f = open("Datasets/charge_moves.csv", "w")
f.write("Move Name,Damage,Energy\n")
for i in range(int(len(moves_text)/grouping_size)):
    f.write(moves_text[grouping_size*i].strip() +  ',' +  moves_text[grouping_size*i+2].strip() + ','+ moves_text[grouping_size*i+3].strip() + '\n')
f.close()
